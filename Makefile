default: docs

.PHONY: docs
docs: README.md

README.md: .terraform-docs.yml *.tf
	@terraform-docs .

.PHONY: check-docs
check-docs:
	@terraform-docs --output-check .

