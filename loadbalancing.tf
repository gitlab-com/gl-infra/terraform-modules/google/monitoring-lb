data "google_compute_lb_ip_ranges" "ranges" {
}

module "dns_record" {
  source  = "ops.gitlab.net/gitlab-com/dns-record/dns"
  version = "3.16.0"

  zone = "gitlab.net."
  a = { for idx, host in var.hosts :
    "${host}.${var.environment}.gitlab.net." => {
      records = [google_compute_global_address.default.address]
      ttl     = 300
    }
  }
}

resource "google_compute_global_address" "default" {
  name = format("%v-%v", var.environment, var.name)
}

resource "google_compute_global_forwarding_rule" "default" {
  name       = format("%v-%v-performance", var.environment, var.name)
  target     = google_compute_target_https_proxy.default.self_link
  port_range = "443"
  ip_address = google_compute_global_address.default.address
}

resource "google_compute_target_https_proxy" "default" {
  name             = format("%v-%v", var.environment, var.name)
  description      = "https proxy for performance"
  ssl_certificates = [var.cert_link]
  url_map          = var.url_map
}

resource "google_compute_firewall" "default" {
  name    = format("%v-%v", var.environment, var.name)
  network = var.environment

  allow {
    protocol = "tcp"
    ports    = var.service_ports
  }

  source_ranges = concat(
    data.google_compute_lb_ip_ranges.ranges.network,
    data.google_compute_lb_ip_ranges.ranges.http_ssl_tcp_internal,
  )
  target_tags = var.targets
}
