# GitLab.com Monitoring Load Balancer Terraform Module

## What is this?

This module provisions a GCE load balancer for monitoring services.

## What is Terraform?

[Terraform](https://www.terraform.io) is an infrastructure-as-code tool that greatly reduces the amount of time needed to implement and scale our infrastructure. It's provider agnostic so it works great for our use case. You're encouraged to read the documentation as it's very well written.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.3 |
| <a name="requirement_google"></a> [google](#requirement\_google) | >= 4.0.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | >= 4.0.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_dns_record"></a> [dns\_record](#module\_dns\_record) | ops.gitlab.net/gitlab-com/dns-record/dns | 3.16.0 |

## Resources

| Name | Type |
|------|------|
| [google_compute_firewall.default](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_firewall) | resource |
| [google_compute_global_address.default](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_global_address) | resource |
| [google_compute_global_forwarding_rule.default](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_global_forwarding_rule) | resource |
| [google_compute_target_https_proxy.default](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_target_https_proxy) | resource |
| [google_compute_lb_ip_ranges.ranges](https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/compute_lb_ip_ranges) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_cert_link"></a> [cert\_link](#input\_cert\_link) | resource link for the ssl certificate | `string` | n/a | yes |
| <a name="input_environment"></a> [environment](#input\_environment) | The environment name | `string` | n/a | yes |
| <a name="input_hosts"></a> [hosts](#input\_hosts) | List of hosts to create A records for. | `list(string)` | n/a | yes |
| <a name="input_name"></a> [name](#input\_name) | The pet name | `string` | n/a | yes |
| <a name="input_service_ports"></a> [service\_ports](#input\_service\_ports) | ports to allow for healthchecks | `list(string)` | n/a | yes |
| <a name="input_targets"></a> [targets](#input\_targets) | target tags for the load balancer | `list(string)` | n/a | yes |
| <a name="input_url_map"></a> [url\_map](#input\_url\_map) | A reference to the UrlMap resource that defines the mapping from URL to the BackendService. | `string` | n/a | yes |

## Outputs

No outputs.
<!-- END_TF_DOCS -->

## Contributing

Please see [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

See [LICENSE](./LICENSE).
