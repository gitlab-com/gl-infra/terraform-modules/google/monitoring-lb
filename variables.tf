variable "url_map" {
  type        = string
  description = "A reference to the UrlMap resource that defines the mapping from URL to the BackendService."
}

variable "hosts" {
  type        = list(string)
  description = "List of hosts to create A records for."
}

variable "service_ports" {
  type        = list(string)
  description = "ports to allow for healthchecks"
}

variable "cert_link" {
  type        = string
  description = "resource link for the ssl certificate"
}

variable "environment" {
  type        = string
  description = "The environment name"
}

variable "name" {
  type        = string
  description = "The pet name"
}

variable "targets" {
  type        = list(string)
  description = "target tags for the load balancer"
}
